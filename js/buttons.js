let newform = document.getElementById("newArtWind");
newform.addEventListener("submit", postArt);

function closeNewartWind() {
  newform.style.display = 'none';
  window.location.href = "index+scss.htm";
}

function postArt(e) {
  e.preventDefault();
  const data = new FormData(e.target);
  let artObj = {};
  data.forEach((value,key) => artObj[key] = value);
  fetch("http://localhost:3000/article/", {
    method:"POST",
    headers:{
      "Content-Type": "application/json"
  },
  body: JSON.stringify(artObj)
  })
  .then((response) => response.json())
  .then(window.location = "index+scss.htm");
}
