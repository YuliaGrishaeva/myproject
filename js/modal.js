let openModal = document.getElementById("openModal");
openModal.addEventListener("click", openModalFunc)
let newform = document.getElementById("newArtWind");
newform.addEventListener("submit", postArt);
let jsModal = document.querySelectorAll('.js_modal');

function openModalFunc(e) {
  e.preventDefault();
  jsModal.forEach((item) => item.classList.add(item.classList[0]+'_show'));
}
function closeNewartWind(e) {
  e.preventDefault();
  jsModal.forEach((item) => item.classList.remove(item.classList[0]+'_show'));
}
function postArt(e) {
  e.preventDefault();
  const data = new FormData(e.target);
  let artObj = {};
  data.forEach((value,key) => artObj[key] = value);
  fetch(ARTICLES_ENDPOINT, {
    method:"POST",
    headers:{
      "Content-Type": "application/json"
  },
  body: JSON.stringify(artObj)
  })
  .then((response) => response.json())
  .then(window.location = "index+scss.htm");
}
