const API_BASE_URL = 'http://localhost:3000/';
const ARTICLES_ENDPOINT = `${API_BASE_URL}article/`;

function addCardDocument(title, content, pic, _id) {
    const cardTemplate = document.getElementById('card-template'); 
    const clone = cardTemplate.content.cloneNode(true);
    clone.querySelector('h1').textContent = title;
    clone.querySelector('p.content').textContent = content;
    clone.querySelector('div.pic').style.background = `url(${pic}) center/cover`;
    clone.querySelector('a').href = ARTICLES_ENDPOINT+`${_id}`;
    return clone; 
}

function addArticle() {
    fetch(ARTICLES_ENDPOINT)
    .then(res => res.json())
    .then(articles => {
        if (articles.length > 0) {
            let block1 = document.getElementById("block1");
            let block2 = document.getElementById("block2");
            let articlesrev = articles.reverse();
            articlesrev.forEach((article, index) => {
                let clone = addCardDocument(article.title, article.content, article.pic, article._id);
                if (index <= 4){
                    block1.append(clone);
                }
                else block2.append(clone);
            });
            stylesArticles();
        }
        else (swal("Нет статей!", "Нажми 'Добавить статью', если это необходимо", "error"));
    })
    .catch((err) => {
        console.log(`ERR: `, err);
    });           
}
addArticle();
function stylesArticles(){
    block1.children[0].classList.add("art1");
    block1.children[0].children[0].classList.add("imgbig");
    block1.children[0].children[1].classList.add("insideart", "top");
    block1.children[0].children[1].children[1].classList.add("text1");
    for (let i = 1; i < block1.children.length; i++) {
        block1.children[i].children[0].classList.add("imgsmall");
        block1.children[i].children[1].classList.add("topsmall");
        block1.children[i].children[1].children[1].classList.add("text2");
        let h1top = block1.children[i].querySelector('h1');
        let h3 = document.createElement('h3');
        h3.textContent = h1top.textContent;
        h1top.replaceWith(h3);
    }
    block1.children[1].classList.add("art2");
    block1.children[2].classList.add("art3");
    block1.children[3].classList.add("art4");
    block1.children[4].classList.add("art5");
    for (let i = 0; i < block2.children.length; i++) {
        block2.children[i].classList.add("art6");
        block2.children[i].children[0].className = "imgside";
        block2.children[i].children[1].classList.add("side");
        block2.children[i].children[1].children[1].classList.add("text3");
        block2.children[i].children[1].children[2].classList.replace("bottom", "bottom2");
        let h1side = block2.children[i].querySelector('h1');  
        let h2 = document.createElement('h2');
        h2.textContent = h1side.textContent;
        h1side.replaceWith(h2);
    }   
}